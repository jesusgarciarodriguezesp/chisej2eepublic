package com.chise.notify;

import java.util.Collection;
import java.util.Vector;

/**
 * Emula un cursor bas�ndse en un vector 
 * @author JesusGarcia
 *
 * 
 *
 */
public class DeviceCursor extends Vector {

	long lquerymaxelements=0;
	long lposition=0;
	
	
	
	/**
	 * Pone el n�mero m�ximo de elementos en la consulta que genera  cursor
	 * @param lqe
	 */
	
	public void setQElements(long lqe)
	{
		lquerymaxelements=lqe;
	}
	
	/**
	 * Obtiene el n�mero m�ximo de elementos en la consulta que genera el cursor
	 * @return Total de elementos de la query
 	 * 
	 */
	public long getQElements()
	{
		return lquerymaxelements;
	}
	
	/**
	 * En qu� posici�n estoy dentro del cursor
	 * 
	 */
	public long getPosition()
	{
		return lposition;
	}
	
	/**
	 * Pone la posicion en la que estoy dentro del cursor
	 * @param lppos
	 */
	public void setPosition(long lppos)
	{
		lposition= lppos; 
	}
	
	public DeviceCursor() {
		// TODO Auto-generated constructor stub
	}

	public DeviceCursor(int arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public DeviceCursor(Collection arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public DeviceCursor(int arg0, int arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

}
