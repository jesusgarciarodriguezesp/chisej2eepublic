/**
 * 
 */
package com.chise.notify;
import java.util.logging.Logger;

/**
 * @author JesusGarcia
 *
 */
public class GoogleNotifyDaemon extends Thread{

	public static long iterationsleep =3000L;
	protected static final GoogleNotifyDaemon basethread = new GoogleNotifyDaemon();
	private final static Logger LOGGER = Logger.getLogger(GoogleNotifyDaemon.class.getName());
	protected static final java.util.ResourceBundle configuration 
	
	   = java.util.ResourceBundle.getBundle("com.chise.notify.config");
	private static boolean btrace = false;
	private static int numsenders=2;
	private static GCMSenderman senders[] = null;
	/**
	 * @param args
	 */
	public static void initdaemon() {
		// TODO Auto-generated method stub
		
		try{
			
			String silconf =configuration.getString("silent");
			
			
			
			
				
			
			
			btrace= silconf==null||silconf.equalsIgnoreCase("n");
			trace("Start main");
			try{
			iterationsleep=Long.parseLong(configuration.getString("sleeptime"));
			trace("Sleep Time is "+iterationsleep);
			}
			catch (Exception em1)
			{
				trace("Sleep time not configured");
			}
			try{
				numsenders=Integer.parseInt(configuration.getString("sendermen"));
			}
			catch (Exception em1)
			{
					trace("Num Sendermen  not configured");
			}
			senders=new GCMSenderman[numsenders];
			for(int i =0;i<numsenders;i++)
			{
				senders[i]=new GCMSenderman();
				
			}
			
			trace("Running loop");
			basethread.start();
		}
		catch (Exception erunscript)
		{
			
		}
		

	}
	
	public static void trace(String msg)
	{
		if(btrace)
		LOGGER.info(msg);
	}
	
	 private boolean checksenders()
	 {
		 boolean senderfull = true;
	        	
	        for(int i=0;i<numsenders;i++)
	        {
	          senderfull = senderfull&& senders[i].isJobStarted()&&!senders[i].isJobDone();	
	        }
	        
	        if(senderfull){
	        	trace("The list of senders is full");
	        	try{
	        		basethread.sleep(iterationsleep);
	        	}
	        	catch (Exception eslp)
	        	{
	        		
	        	}
	        }
	        
	        
	        return !senderfull;
	 }
	
	 public void run() {
	        trace("Main Thread start: ");
	       
	        
	        try{
	        int cursenderman =0;
	        int debugnumtries =0;
	        
	        while((debugnumtries<30&&btrace)||!btrace)
	        {
	        	
	        trace(" Main loop ");
	        while	(cursenderman<numsenders&&!checksenders());
	        {
	        
	        trace(" Senders loop "+cursenderman+" of "+numsenders);	
	        
	        java.util.Vector vjobs=DataManager.getPendingJobs("gcm");	
	        int jidexe = 0;
	        
	        while(!vjobs.isEmpty()&&(jidexe<vjobs.size()))
	        {
	        	
	        	while(cursenderman<numsenders&&senders[cursenderman].isJobStarted())
	        	{
	        		if (senders[cursenderman].isJobDone())
	        		{
	        			trace("Renew Thread "+cursenderman);
	        			senders[cursenderman]=new GCMSenderman();
	        		}
	        		else
	        		cursenderman++;
	        	}
	        	
	        	if(cursenderman>=numsenders)cursenderman =0;
	        	
	        	if(!senders[cursenderman].isJobStarted())
	        	{	java.util.HashMap hmapx = null;
	        	    if(jidexe<vjobs.size()){
	        	    hmapx=(java.util.HashMap) vjobs.elementAt(jidexe);
	        	    
	        	
	        	    try{
	        		senders[cursenderman].doJob((String)hmapx.get("work"),(String)hmapx.get("appcode"));
	        	    }
	        	    catch (Exception esender)
	        	    {
	        	    	LOGGER.severe("Error senderman for job"+hmapx);
	        	    }
	        	    
	        	    DataManager.setJobFinished((String)hmapx.get("sidwork") );
	        	    }
	        	    
	        		jidexe++;
	        	}
	        	debugnumtries++;
	        }//  while pending jobs
	        
	        if(vjobs.isEmpty())
	        {
	        	try{
	        		trace("Main thread is sleeping again");
	        		basethread.sleep(5000);
		        	trace("Main thread is running again");
		        	}
		        	catch (Exception eslp)
		        	{
		        		
		        	}
	        }
	        
	        
	        }
	        
	        
	        // Sleep between scan
	        trace("Main Thread frozen between scans ");
	        if(checksenders())
	        DataManager.refreshconn();
	        sleep(iterationsleep);
	        
	        }
	        
	        }
	        catch(Exception ersleep)
	        {
	        	trace("Main thread  failed :"+ersleep.toString());
	        }
	        trace("Main Thread awake ");
	    }

}
