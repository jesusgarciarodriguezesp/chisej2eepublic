package com.chise.notify;

import java.util.logging.Logger;

public class GCMSenderman extends Thread {

	private final static Logger LOGGER = Logger.getLogger(GCMSenderman.class.getName());
	private static boolean btrace = false;
	protected static final java.util.ResourceBundle configuration 
	
	   = java.util.ResourceBundle.getBundle("com.chise.notify.config");
	static
	{
		String silconf =configuration.getString("silent");
		btrace= silconf==null||silconf.equalsIgnoreCase("n");
	}
	private String setupjob ="undefined";
    private String setupapp ="allapps";
    private boolean jobstarted =false;
    private boolean jobfinished =false;
    private boolean jobstatus =true;
    
    public GCMSenderman() {
		// TODO Auto-generated constructor stub
	}

	public void doJob(String job,String appcode)
	{
		setupjob=job;
		setupapp =appcode;
		jobstatus =true;
		try
		{
		start();
		}
		catch (Exception ejob)
    	{
			LOGGER.severe(" Error in job exec "+ejob.getMessage());	
    	}
		
	}
	
	public boolean isJobStarted()
	{
		return jobstarted;
	}
    
	public boolean isJobDone()
	{
		return jobfinished;
	}
	 public void run() {
		 jobstarted=true;
		 try{
		 trace(" job started "+setupjob);
		 String scontent= ContentManager.getContent(setupjob);
		 if(scontent!=null&&scontent.length()>3)
		 trace(" Content get ..."+scontent.substring(scontent.length()-3));
		 DeviceCursor devc = DataManager.getAppDevicesCursor(setupapp,"gcm");
		 trace(" Getting are some devices"+setupapp+" gcm");
		 int numerrors =0;
		 while(!devc.isEmpty())
	      {
			 trace(" There are some devices"+devc.size());
			 int curdestination=0;
			 while(curdestination<devc.size())
			 {
				 String sdest = (String) devc.elementAt(curdestination);
				 if(sdest!=null)
				 {
				 
				 if(sdest!=null&&sdest.length()>3)	 
				 trace("Sending to the destination ..."+sdest.substring(sdest.length()-3));
				 
				 try{
					 GCMMessenger meseng = new GCMMessenger();
					 if(scontent!=null&&sdest!=null)
					 {
					 meseng.transfer(sdest, scontent);
					 if(scontent!=null&&sdest!=null&&scontent.length()>3&&sdest.length()>3)
					 trace(" Transfer this content ..."+scontent.substring(scontent.length()-3)+" to ..."+sdest.substring(sdest.length()-3)+" ");
					 }
					 else
						 LOGGER.severe("Content or destination is null  ");
					 
					 
				 }catch (Exception edenevio)
				 {
					 LOGGER.severe("Send Fail  "+edenevio);
					 numerrors++;
					 if(numerrors>(devc.getQElements()/4))
					 {
						 jobstatus =false;
						 LOGGER.severe("Send Fail too much errors >%25 TOT");
					 }
				 }
				 }
				 curdestination++;
				 trace("New destination "+curdestination+" of "+devc.size());
			 }	 
			 trace("Act Devices"+devc.getPosition()+ devc.getQElements());
			 devc=DataManager.nextAppDevices(devc,setupapp,"gcm");
			 trace("Next devices"+devc.getPosition()+ devc.getQElements());
		 }
		
		 
		 }
		 catch(Exception ejob)
		 {
			 LOGGER.severe("Send Fail "+ejob.toString());
			 jobstatus =false;
		 }
		 
		 jobfinished=true;
	        
	 }

	 public static void trace(String msg)
		{
			if(btrace)
			LOGGER.info(msg);
		}

}
