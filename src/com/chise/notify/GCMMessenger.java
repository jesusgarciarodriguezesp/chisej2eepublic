package com.chise.notify;

import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpHeaders;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

/*
 * https://developer.android.com/google/gcm/http.html
 * https://developer.android.com/google/gcm/gs.html
 * 
 * Authorization: key=YOUR_API_KEY
Content-Type: application/json for JSON; application/x-www-form-urlencoded;charset=UTF-8 for plain text. If Content-Type is omitted, the format is assumed to be plain text.
 *  
 *  
 */


public class GCMMessenger {
	
	private final static Logger LOGGER = Logger.getLogger(GCMMessenger.class.getName());
	private static boolean btrace = false;
	protected static final java.util.ResourceBundle configuration 
	
	   = java.util.ResourceBundle.getBundle("com.chise.notify.config");
	static
	{
		String silconf =configuration.getString("silent");
		btrace= silconf==null||silconf.equalsIgnoreCase("n");
	}
	public GCMMessenger() {
	
		// TODO Auto-generated constructor stub
	}
	
	public void transfer(String dest,String mess)
	{
		//google_secret = "googlekeysecret"
		//google_enpoint = "http://localhost/"
		
		trace("Init Transfer ");
		try
		{
			
		// test
		mess=org.apache.commons.lang3.StringEscapeUtils.escapeJson(mess)	;
		// mi test
		//mess=" Error tio";
		String gsecret =configuration.getString("google_secret");
		String gendpont =configuration.getString("google_enpoint");
		String jsonmess ="{ \"data\": { \"mess\": \""+mess+"\"";
		jsonmess+="}, \"registration_ids\": [\""+dest+"\"]}";
		
		
		DefaultHttpClient httpclient = new DefaultHttpClient();

        HttpPost httppostme = new HttpPost(gendpont);
        httppostme.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        httppostme.setHeader(HttpHeaders.ACCEPT_ENCODING, "application/json");
        httppostme.setHeader(HttpHeaders.AUTHORIZATION, "key="+gsecret);
        trace(" Send json with content in mess variable json  size:  "+jsonmess.length());
        //.getBytes("UTF-8")
        StringEntity input = new StringEntity(jsonmess);
		//input.setContentType("application/json");
		httppostme.setEntity(input);
		Header [] hposttrace = httppostme.getAllHeaders();
		for (int conta=0;hposttrace!=null&&conta<hposttrace.length;conta++)
        trace("All headers   "+hposttrace[conta]);
        
		HttpResponse response = httpclient.execute(httppostme);
		 
		
		int rescod= response.getStatusLine().getStatusCode(); 
		if (rescod != 201 && rescod!=200 ) {
			LOGGER.severe("Failed :"+gendpont+" HTTP error code : "
				+ response.getStatusLine().getStatusCode());
		}
		else
		{
		java.io.BufferedReader br = new java.io.BufferedReader(
                        new java.io.InputStreamReader((response.getEntity().getContent())));
 
		String output;
		trace("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			LOGGER.info(output);
		}
		}
		
		httpclient.getConnectionManager().shutdown();
 
        
        //Authorization:key=AIzaSyB-1uEai2WiUapxCs2Q0GZYzPu7Udno5aA

        
       
        
		}
		catch (Exception errordeenvio)
		{
			LOGGER.severe("Transfer Error "+errordeenvio.getMessage());
			errordeenvio.printStackTrace();
					
		}
		
	}
	
	public static void trace(String msg)
	{
		if(btrace)
		LOGGER.info(msg);
	}
}
