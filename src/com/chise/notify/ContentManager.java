package com.chise.notify;

import java.net.*;
import java.io.*;

public  class ContentManager {

	public ContentManager() {
		// TODO Auto-generated constructor stub
	}

	public static String getContent(String contentcode)
	{
		String scontent="Default empty content";
		if(contentcode!=null&&contentcode.startsWith("http")) 
		{
			try{
			 
			 URL uconten = new URL(contentcode);
		     URLConnection yc = uconten.openConnection();
		        BufferedReader in = new BufferedReader(new InputStreamReader(
		                                    yc.getInputStream()));
		        String inputLine= in.readLine();
		        scontent="";
		        do
		        { if(inputLine!=null)
		        	scontent+=inputLine;
		        } 		
		        while ((inputLine = in.readLine()) != null);
		        in.close();
			}
			catch (Exception econnurl)
			{
				econnurl.printStackTrace();
			}
		}
		return scontent;
	}
}
