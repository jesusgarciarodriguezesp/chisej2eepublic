package com.chise.notify;

import java.util.logging.Logger;

/**
 * 
 * Clase estatica de metodos de acceso a datos . Hace pooling sobre una conexion sql 
 *  para obtener los datos . Maneja un cursor propio para  movenos por los resultados de los registros
 * 
 * @author JesusGarcia
 *
 */
public  class DataManager {

private final static Logger LOGGER = Logger.getLogger(DataManager.class.getName());
private static boolean btrace = false;
protected static final java.util.ResourceBundle configuration 

= java.util.ResourceBundle.getBundle("com.chise.notify.config");
static
{
	String silconf =configuration.getString("silent");
	btrace= silconf==null||silconf.equalsIgnoreCase("n");
}
public static String	db_host = "localhost";
public static int	db_port = 3306;
public static String	db_user = "orniguia_user";
public static String  db_schema = "orniguiasiberia_bbdd";
public static String	db_password  ="2fymY9~3";
public static String	driver  ="com.mysql.jdbc.Driver";

public static java.sql.Connection conn=null;
public static Boolean blockaccess = new Boolean(false);
public static int sqlworks =0;



    /**
     *  Entra dentro de una conexion en modo thrtead safe
     */
    private static void enterconn() 
    { 
    	if(conn==null)
    	synchronized(blockaccess)
    	{
    	 	
    		loadconn() ;
    	  
    	}
    	
    	sqlworks++;
    }
    
    /**
     *  Sale  de una conexion en modo thrtead safe
     */
    
    private static void leaveconn() 
    { 
 
    	sqlworks--;
    }
    
    
	/**
	 * Carga la conexcion o la refcarga
	 */
	private static void loadconn() {
		if(conn!=null)
		{
			try
			{
				conn.close();
			}
			catch (Exception esqlc)
			{
			  LOGGER.severe("Sql error close:"+esqlc);	
			}
			conn=null;
		}
		
		try{
			
		db_host =	 configuration.getString("db_host"); 
		String strdb_port =	 configuration.getString("db_port");
		if(strdb_port!=null)
		{
			try{
				db_port=Integer.parseInt(strdb_port);
			}
			catch(Exception enumber)
			{}
		}
		db_user =	 configuration.getString("db_user");
		db_schema =	 configuration.getString("db_schema");
		 try {

			// Load the MySQL JDBC driver
			      Class.forName("com.mysql.jdbc.Driver") ;
			      trace("MySQL JDBC driver loaded ok.");
			 
			    } catch (Exception e) {
			      LOGGER.severe("Exception loading driver: "+e.getMessage());
			    }
		
		//java.sql.Driver jdriver=	java.sql.DriverManager.getDriver(driver);
		String  jdbcurlis= "jdbc:mysql://"+db_host+":"+db_port+"/"+db_schema+"?user="+db_user+"&password="+db_password;
		conn=java.sql.DriverManager.getConnection(jdbcurlis);
		}
		catch (Exception ederv)
		{
			LOGGER.severe("Sql error drv:");	
			ederv.printStackTrace();
		}
	  }
	
	/**
	 *  o la recarga
	 */
	public static void refreshconn() 
    { 
		if(sqlworks==0)
    	synchronized(blockaccess)
    	{
    	  if(sqlworks==0)
    	  loadconn();
    	  	
    	  
    	}
		trace("Refresh sql conn executed numworks"+sqlworks);
    }
	
	
	
	public static java.util.Vector getPendingJobs(String nottype)
	
	{
		java.util.Vector jobvector = new java.util.Vector();
		enterconn() ;
		if(conn!=null)
		try
		{
		 java.sql.Statement stmt = conn.createStatement();
		 java.sql.ResultSet rs;
         String squery ="SELECT * FROM `notify_gc` where `status`=0 and  `not_type`=\""+nottype+"\"  ORDER BY `not_ts` ASC";
         trace(squery);
         rs = stmt.executeQuery(squery);
         while (rs!=null&& rs.next() ) {
        	 
        	 
        	 java.util.HashMap hmapx = new java.util.HashMap();
        	 
             String swork = rs.getString("work_url");
             String sidwork = rs.getString("id_work");
             String sappc = rs.getString("appcode");
             
             hmapx.put("sidwork",sidwork);
             hmapx.put("work",swork);
             hmapx.put("appcode", sappc);
             
             jobvector.add(hmapx);
        	 
         }
         rs.close();
         stmt.close();
		}
		catch (Exception esqlpj)
		{
			LOGGER.severe("Error getting pending jobs "+esqlpj); 
			esqlpj.printStackTrace();	
		}
		
		leaveconn() ;
		return jobvector;
	}
	
	public static void setJobFinished(String sidwork )
	{
		enterconn() ;
		if(conn!=null)
	    try
		{
		 java.sql.Statement stmt = conn.createStatement();
	     String sql = "UPDATE `notify_gc` set `status`=1 where `id_work`="+sidwork;
	     stmt.executeUpdate(sql);
	     stmt.close();
		}
		catch (Exception esqlpj)
		{
			LOGGER.severe("Error setting job status "+esqlpj); 	
			esqlpj.printStackTrace();
		}
		
		leaveconn() ;
	
	}
	
	
	public static DeviceCursor getAppDevicesCursor(String appcode,String regtype)
	{
		DeviceCursor jobvector = new DeviceCursor();
		nextAppDevices(jobvector,appcode,regtype);
		return jobvector;
	}
	
	
	
	public static DeviceCursor nextAppDevices(DeviceCursor jobvector,String appcode,String regtype)
	{
		
		if(0<jobvector.getPosition()&&jobvector.getPosition()>=jobvector.getQElements())
			
		{
			jobvector.clear();
			trace("Clean vector there is no more data");
			return jobvector;
		}	
		enterconn() ;
		String squerytot ="SELECT 0";
	     
		trace( "app "+appcode+" reg"+regtype);
		
		
		try
		{
		 if(regtype==null)
			 regtype="gcm";
		    
		jobvector.clear();	
		 java.sql.Statement stmt = conn.createStatement();
		 java.sql.ResultSet rs;
         String squery = "SELECT * FROM `registry_gc` where `reg_type`=\""+regtype+"\" LIMIT ";
         squerytot ="SELECT COUNT(*) FROM `registry_gc` where `reg_type`=\""+regtype+"\" ";
 	     
         long curposition = 0;
         int pagepos=0;
         if (appcode!=null && !appcode.trim().equals("") && ! appcode.equals("allapps"))
         {
        	 squery = "SELECT * FROM `registry_gc` where `reg_type`=\""+regtype+"\" and  `appcode`=\""+appcode+"\" LIMIT ";
        	 squerytot ="SELECT COUNT(*) FROM `registry_gc` where `reg_type`=\""+regtype+"\" and  `appcode`=\""+appcode+"\"";
         }
         
		 // rownum BETWEEN ? and ?
         // limit offset, count
		 
         
         curposition=jobvector.getPosition();
         String slimits = ""+curposition+" , "+100;
         squery+=slimits;
         if(curposition==0 || curposition%100==0)
         {	 
        
         trace(" Execute "+squery);
         rs = stmt.executeQuery(squery);
         
         
         while ( rs.next()&&pagepos<100 ) {
        	 
        	 String snotifyto=rs.getString("id_gc");
             trace(" Device found"+snotifyto);
             jobvector.add(snotifyto);
             curposition++;
             pagepos++;
             jobvector.setPosition(curposition);
             
             //curposition ++;
            
         }
         trace(" Fetch position "+curposition);
         rs.close();
         
         stmt.close();
         }
		}
		catch (Exception esqlpj)
		{
			LOGGER.severe("Error getting app devices "+appcode+" at "+jobvector.getPosition()+" "+esqlpj.getMessage());
			esqlpj.printStackTrace();
		}
		
		if(jobvector.getQElements()==0)
		{
			try{
				trace ("Get Q elements");
				java.sql.Statement stmt = conn.createStatement();
				 java.sql.ResultSet rs;
		         
		         
				 
				 
		         rs = stmt.executeQuery(squerytot);
		         if(rs.next())
		         {
		        	 jobvector.setQElements(rs.getLong(1));
		         }
		         
		         LOGGER.info(" Num elements in job : "+jobvector.getQElements());
		         
		         rs.close();
		         
		         stmt.close();
				
			}
			catch (Exception esqlpj)
			{
				LOGGER.severe("Error getting MAX devices "+esqlpj.getMessage()); 	
				esqlpj.printStackTrace();
			}
		}	
		
		leaveconn() ;
		return jobvector;
	}
	
	public static void trace(String msg)
	{
		if(btrace)
		LOGGER.info(msg);
	}
	
	

}
