#!/bin/sh

# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# -----------------------------------------------------------------------------
# Start/Stop Script for the CATALINA Server
#
# Environment Variable Prerequisites
#
#   NOTIFY_HOME   May point at your Catalina "build" directory.
#
#   NOTIFY_BASE   (Optional) Base directory for resolving dynamic portions
#                   of a Catalina installation.  If not present, resolves to
#                   the same directory that NOTIFY_HOME points to.
#
#   NOTIFY_OUT    (Optional) Full path to a file where stdout and stderr
#                   will be redirected.
#                   Default is $NOTIFY_BASE/logs/notify.out
#
#   NOTIFY_OPTS   (Optional) Java runtime options used when the "start",
#                   or "run" command is executed.
#
#   NOTIFY_TMPDIR (Optional) Directory path location of temporary directory
#                   the JVM should use (java.io.tmpdir).  Defaults to
#                   $NOTIFY_BASE/temp.
#
#   JAVA_HOME       Must point at your Java Development Kit installation.
#                   Required to run the with the "debug" argument.
#
#   JRE_HOME        Must point at your Java Development Kit installation.
#                   Defaults to JAVA_HOME if empty.
#
#   JAVA_OPTS       (Optional) Java runtime options used when the "start",
#                   "stop", or "run" command is executed.
#
#   JAVA_ENDORSED_DIRS (Optional) Lists of of colon separated directories
#                   containing some jars in order to allow replacement of APIs
#                   created outside of the JCP (i.e. DOM and SAX from W3C).
#                   It can also be used to update the XML parser implementation.
#                   Defaults to $NOTIFY_HOME/endorsed.
#
#   JPDA_TRANSPORT  (Optional) JPDA transport used when the "jpda start"
#                   command is executed. The default is "dt_socket".
#
#   JPDA_ADDRESS    (Optional) Java runtime options used when the "jpda start"
#                   command is executed. The default is 8000.
#
#   JPDA_SUSPEND    (Optional) Java runtime options used when the "jpda start"
#                   command is executed. Specifies whether JVM should suspend
#                   execution immediately after startup. Default is "n".
#
#   JPDA_OPTS       (Optional) Java runtime options used when the "jpda start"
#                   command is executed. If used, JPDA_TRANSPORT, JPDA_ADDRESS,
#                   and JPDA_SUSPEND are ignored. Thus, all required jpda
#                   options MUST be specified. The default is:
#
#                   -agentlib:jdwp=transport=$JPDA_TRANSPORT,
#                       address=$JPDA_ADDRESS,server=y,suspend=$JPDA_SUSPEND
#
#   NOTIFY_PID    (Optional) Path of the file which should contains the pid
#                   of catalina startup java process, when start (fork) is used
#
#   LOGGING_CONFIG  (Optional) Override Process's logging config file
#                   Example (all one line)
#                   LOGGING_CONFIG="-Djava.util.logging.config.file=$NOTIFY_BASE/conf/logging.properties"
#
#   LOGGING_MANAGER (Optional) Override Process's logging manager
#                   Example (all one line)
#                   LOGGING_MANAGER="-Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager"
#
# $Id: notify.sh 1040547 2010-11-30 14:47:49Z markt $
# -----------------------------------------------------------------------------

# OS specific support.  $var _must_ be set to either true or false.
cygwin=false
os400=false
darwin=false
case "`uname`" in
CYGWIN*) cygwin=true;;
OS400*) os400=true;;
Darwin*) darwin=true;;
esac

# resolve links - $0 may be a softlink
PRG="$0"

while [ -h "$PRG" ]; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`

# Only set NOTIFY_HOME if not already set
[ -z "$NOTIFY_HOME" ] && NOTIFY_HOME=`cd "$PRGDIR/.." >/dev/null; pwd`

# Ensure that any user defined CLASSPATH variables are not used on startup,
# but allow them to be specified in setenv.sh, in rare case when it is needed.
CLASSPATH=

if [ -r "$NOTIFY_BASE"/bin/setenv.sh ]; then
  . "$NOTIFY_BASE"/bin/setenv.sh
elif [ -r "$NOTIFY_HOME"/bin/setenv.sh ]; then
  . "$NOTIFY_HOME"/bin/setenv.sh
fi

# For Cygwin, ensure paths are in UNIX format before anything is touched
if $cygwin; then
  [ -n "$JAVA_HOME" ] && JAVA_HOME=`cygpath --unix "$JAVA_HOME"`
  [ -n "$JRE_HOME" ] && JRE_HOME=`cygpath --unix "$JRE_HOME"`
  [ -n "$NOTIFY_HOME" ] && NOTIFY_HOME=`cygpath --unix "$NOTIFY_HOME"`
  [ -n "$NOTIFY_BASE" ] && NOTIFY_BASE=`cygpath --unix "$NOTIFY_BASE"`
  [ -n "$CLASSPATH" ] && CLASSPATH=`cygpath --path --unix "$CLASSPATH"`
fi

# For OS400
if $os400; then
  # Set job priority to standard for interactive (interactive - 6) by using
  # the interactive priority - 6, the helper threads that respond to requests
  # will be running at the same priority as interactive jobs.
  COMMAND='chgjob job('$JOBNAME') runpty(6)'
  system $COMMAND

  # Enable multi threading
  export QIBM_MULTI_THREADED=Y
fi

# Get standard Java environment variables
if $os400; then
  # -r will Only work on the os400 if the files are:
  # 1. owned by the user
  # 2. owned by the PRIMARY group of the user
  # this will not work if the user belongs in secondary groups
  BASEDIR="$NOTIFY_HOME"
  . "$NOTIFY_HOME"/bin/setclasspath.sh
else
  if [ -r "$NOTIFY_HOME"/bin/setclasspath.sh ]; then
    BASEDIR="$NOTIFY_HOME"
    . "$NOTIFY_HOME"/bin/setclasspath.sh
  else
    echo "Cannot find $NOTIFY_HOME/bin/setclasspath.sh"
    echo "This file is needed to run this program"
    exit 1
  fi
fi

if [ -z "$NOTIFY_BASE" ] ; then
  NOTIFY_BASE="$NOTIFY_HOME"
fi


if [ ! -z "$CLASSPATH" ] ; then
  CLASSPATH="$CLASSPATH":
fi
if [ "$NOTIFY_BASE" != "$NOTIFY_HOME" ] && [ -r "$NOTIFY_BASE/bin/tomcat-juli.jar" ] ; then
  CLASSPATH="$CLASSPATH""$NOTIFY_BASE"/lib/notify:"$NOTIFY_HOME"/lib/notify.jar
else
  CLASSPATH="$CLASSPATH""$NOTIFY_HOME"/lib/notify.jar
fi

if [ -z "$NOTIFY_OUT" ] ; then
  NOTIFY_OUT="$NOTIFY_BASE"/logs/notify.out
fi

if [ -z "$NOTIFY_TMPDIR" ] ; then
  # Define the java.io.tmpdir to use for Catalina
  NOTIFY_TMPDIR="$NOTIFY_BASE"/temp
fi

# Bugzilla 37848: When no TTY is available, don't output to console
have_tty=0
if [ "`tty`" != "not a tty" ]; then
    have_tty=1
fi

# For Cygwin, switch paths to Windows format before running java
if $cygwin; then
  JAVA_HOME=`cygpath --absolute --windows "$JAVA_HOME"`
  JRE_HOME=`cygpath --absolute --windows "$JRE_HOME"`
  NOTIFY_HOME=`cygpath --absolute --windows "$NOTIFY_HOME"`
  NOTIFY_BASE=`cygpath --absolute --windows "$NOTIFY_BASE"`
  NOTIFY_TMPDIR=`cygpath --absolute --windows "$NOTIFY_TMPDIR"`
  CLASSPATH=`cygpath --path --windows "$CLASSPATH"`
  JAVA_ENDORSED_DIRS=`cygpath --path --windows "$JAVA_ENDORSED_DIRS"`
fi

# Set juli LogManager config file if it is present and an override has not been issued
if [ -z "$LOGGING_CONFIG" ]; then
  if [ -r "$NOTIFY_BASE"/conf/logging.properties ]; then
    LOGGING_CONFIG="-Djava.util.logging.config.file=$NOTIFY_BASE/conf/logging.properties"
  else
    # Bugzilla 45585
    LOGGING_CONFIG="-Dnop"
  fi
fi

if [ -z "$LOGGING_MANAGER" ]; then
  JAVA_OPTS="$JAVA_OPTS -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager"
else
  JAVA_OPTS="$JAVA_OPTS $LOGGING_MANAGER"
fi

# ----- Execute The Requested Command -----------------------------------------

# Bugzilla 37848: only output this if we have a TTY
if [ $have_tty -eq 1 ]; then
  echo "Using NOTIFY_BASE:   $NOTIFY_BASE"
  echo "Using NOTIFY_HOME:   $NOTIFY_HOME"
  echo "Using NOTIFY_TMPDIR: $NOTIFY_TMPDIR"
  if [ "$1" = "debug" ] ; then
    echo "Using JAVA_HOME:       $JAVA_HOME"
  else
    echo "Using JRE_HOME:        $JRE_HOME"
  fi
  echo "Using CLASSPATH:       $CLASSPATH"
  if [ ! -z "$NOTIFY_PID" ]; then
    echo "Using NOTIFY_PID:    $NOTIFY_PID"
  fi
fi

if [ "$1" = "jpda" ] ; then
  if [ -z "$JPDA_TRANSPORT" ]; then
    JPDA_TRANSPORT="dt_socket"
  fi
  if [ -z "$JPDA_ADDRESS" ]; then
    JPDA_ADDRESS="8000"
  fi
  if [ -z "$JPDA_SUSPEND" ]; then
    JPDA_SUSPEND="n"
  fi
  if [ -z "$JPDA_OPTS" ]; then
    JPDA_OPTS="-agentlib:jdwp=transport=$JPDA_TRANSPORT,address=$JPDA_ADDRESS,server=y,suspend=$JPDA_SUSPEND"
  fi
  NOTIFY_OPTS="$NOTIFY_OPTS $JPDA_OPTS"
  shift
fi

if [ "$1" = "debug" ] ; then
  if $os400; then
    echo "Debug command not available on OS400"
    exit 1
  else
    shift
    if [ "$1" = "-security" ] ; then
      if [ $have_tty -eq 1 ]; then
        echo "Using Security Manager"
      fi
      shift
      exec "$_RUNJDB" "$LOGGING_CONFIG" $JAVA_OPTS $NOTIFY_OPTS \
        -Djava.endorsed.dirs="$JAVA_ENDORSED_DIRS" -classpath "$CLASSPATH" \
        -sourcepath "$NOTIFY_HOME"/../../java \
        -Djava.security.manager \
        -Djava.security.policy=="$NOTIFY_BASE"/conf/notify.policy \
        -Dnotify.base="$NOTIFY_BASE" \
        -Dnotify.home="$NOTIFY_HOME" \
        -Djava.io.tmpdir="$NOTIFY_TMPDIR" \
        com.chise.notify.Notifier "$@" start
    else
      exec "$_RUNJDB" "$LOGGING_CONFIG" $JAVA_OPTS $NOTIFY_OPTS \
        -Djava.endorsed.dirs="$JAVA_ENDORSED_DIRS" -classpath "$CLASSPATH" \
        -sourcepath "$NOTIFY_HOME"/../../java \
        -Dnotify.base="$NOTIFY_BASE" \
        -Dnotify.home="$NOTIFY_HOME" \
        -Djava.io.tmpdir="$NOTIFY_TMPDIR" \
        com.chise.notify.Notifier "$@" start
    fi
  fi

elif [ "$1" = "run" ]; then

  shift
  if [ "$1" = "-security" ] ; then
    if [ $have_tty -eq 1 ]; then
      echo "Using Security Manager"
    fi
    shift
    exec "$_RUNJAVA" "$LOGGING_CONFIG" $JAVA_OPTS $NOTIFY_OPTS \
      -Djava.endorsed.dirs="$JAVA_ENDORSED_DIRS" -classpath "$CLASSPATH" \
      -Djava.security.manager \
      -Djava.security.policy=="$NOTIFY_BASE"/conf/notify.policy \
      -Dnotify.base="$NOTIFY_BASE" \
      -Dnotify.home="$NOTIFY_HOME" \
      -Djava.io.tmpdir="$NOTIFY_TMPDIR" \
      com.chise.notify.Notifier "$@" start
  else
    exec "$_RUNJAVA" "$LOGGING_CONFIG" $JAVA_OPTS $NOTIFY_OPTS \
      -Djava.endorsed.dirs="$JAVA_ENDORSED_DIRS" -classpath "$CLASSPATH" \
      -Dnotify.base="$NOTIFY_BASE" \
      -Dnotify.home="$NOTIFY_HOME" \
      -Djava.io.tmpdir="$NOTIFY_TMPDIR" \
      com.chise.notify.Notifier "$@" start
  fi

elif [ "$1" = "start" ] ; then

  if [ ! -z "$NOTIFY_PID" ]; then
    if [ -f "$NOTIFY_PID" ]; then
      if [ -s "$NOTIFY_PID" ]; then
        echo "Existing PID file found during start."
        if [ -r "$NOTIFY_PID" ]; then
          PID=`cat "$NOTIFY_PID"`
          ps -p $PID >/dev/null 2>&1
          if [ $? -eq 0 ] ; then
            echo "Process appears to still be running with PID $PID. Start aborted."
            exit 1
          else
            echo "Removing/clearing stale PID file."
            rm -f "$NOTIFY_PID" >/dev/null 2>&1
            if [ $? != 0 ]; then
              if [ -w "$NOTIFY_PID" ]; then
                cat /dev/null > "$NOTIFY_PID"
              else
                echo "Unable to remove or clear stale PID file. Start aborted."
                exit 1
              fi
            fi
          fi
        else
          echo "Unable to read PID file. Start aborted."
          exit 1
        fi
      else
        rm -f "$NOTIFY_PID" >/dev/null 2>&1
        if [ $? != 0 ]; then
          if [ ! -w "$NOTIFY_PID" ]; then
            echo "Unable to remove or write to empty PID file. Start aborted."
            exit 1
          fi
        fi
      fi
    fi
  fi

  shift
  touch "$NOTIFY_OUT"
  if [ "$1" = "-security" ] ; then
    if [ $have_tty -eq 1 ]; then
      echo "Using Security Manager"
    fi
    shift
    "$_RUNJAVA" "$LOGGING_CONFIG" $JAVA_OPTS $NOTIFY_OPTS \
      -Djava.endorsed.dirs="$JAVA_ENDORSED_DIRS" -classpath "$CLASSPATH" \
      -Djava.security.manager \
      -Djava.security.policy=="$NOTIFY_BASE"/conf/notify.policy \
      -Dnotify.base="$NOTIFY_BASE" \
      -Dnotify.home="$NOTIFY_HOME" \
      -Djava.io.tmpdir="$NOTIFY_TMPDIR" \
      com.chise.notify.Notifier "$@" start \
      >> "$NOTIFY_OUT" 2>&1 &

  else
    "$_RUNJAVA" "$LOGGING_CONFIG" $JAVA_OPTS $NOTIFY_OPTS \
      -Djava.endorsed.dirs="$JAVA_ENDORSED_DIRS" -classpath "$CLASSPATH" \
      -Dnotify.base="$NOTIFY_BASE" \
      -Dnotify.home="$NOTIFY_HOME" \
      -Djava.io.tmpdir="$NOTIFY_TMPDIR" \
      com.chise.notify.Notifier "$@" start \
      >> "$NOTIFY_OUT" 2>&1 &

  fi

  if [ ! -z "$NOTIFY_PID" ]; then
    echo $! > "$NOTIFY_PID"
  fi

elif [ "$1" = "stop" ] ; then

  shift

  SLEEP=5
  if [ ! -z "$1" ]; then
    echo $1 | grep "[^0-9]" >/dev/null 2>&1
    if [ $? -gt 0 ]; then
      SLEEP=$1
      shift
    fi
  fi

  FORCE=0
  if [ "$1" = "-force" ]; then
    shift
    FORCE=1
  fi

  if [ ! -z "$NOTIFY_PID" ]; then
    if [ -s "$NOTIFY_PID" ]; then
      if [ -f "$NOTIFY_PID" ]; then
        kill -0 `cat "$NOTIFY_PID"` >/dev/null 2>&1
        if [ $? -gt 0 ]; then
          echo "PID file found but no matching process was found. Stop aborted."
          exit 1
        fi
      else
        echo "\$NOTIFY_PID was set but the specified file does not exist. Is Process running? Stop aborted."
        exit 1
      fi
    else
      echo "PID file is empty and has been ignored."
    fi
  fi
  
  "$_RUNJAVA" $JAVA_OPTS \
    -Djava.endorsed.dirs="$JAVA_ENDORSED_DIRS" -classpath "$CLASSPATH" \
    -Dnotify.base="$NOTIFY_BASE" \
    -Dnotify.home="$NOTIFY_HOME" \
    -Djava.io.tmpdir="$NOTIFY_TMPDIR" \
    com.chise.notify.Notifier "$@" stop

  if [ ! -z "$NOTIFY_PID" ]; then
    if [ -f "$NOTIFY_PID" ]; then
      while [ $SLEEP -ge 0 ]; do
        kill -0 `cat "$NOTIFY_PID"` >/dev/null 2>&1
        if [ $? -gt 0 ]; then
          rm -f "$NOTIFY_PID" >/dev/null 2>&1
          if [ $? != 0 ]; then
            if [ -w "$NOTIFY_PID" ]; then
              cat /dev/null > "$NOTIFY_PID"
            else
              echo "Process stopped but the PID file could not be removed or cleared."
            fi
          fi
          break
        fi
        if [ $SLEEP -gt 0 ]; then
          sleep 1
        fi
        if [ $SLEEP -eq 0 ]; then
          if [ $FORCE -eq 0 ]; then
            echo "Process did not stop in time. PID file was not removed."
          fi
        fi
        SLEEP=`expr $SLEEP - 1 `
      done
    fi
  fi

  if [ $FORCE -eq 1 ]; then
    if [ -z "$NOTIFY_PID" ]; then
      echo "Kill failed: \$NOTIFY_PID not set"
    else
      if [ -f "$NOTIFY_PID" ]; then
        PID=`cat "$NOTIFY_PID"`
        echo "Killing Process with the PID: $PID"
        kill -9 $PID
        rm -f "$NOTIFY_PID" >/dev/null 2>&1
        if [ $? != 0 ]; then
          echo "Process was killed but the PID file could not be removed."
        fi
      fi
    fi
  fi

elif [ "$1" = "version" ] ; then

    "$_RUNJAVA"   \
      -classpath "$NOTIFY_HOME/lib/notify.jar" \
      com.chise.notify.Notifier

else

  echo "Usage: notify.sh ( commands ... )"
  echo "commands:"
  if $os400; then
    echo "  debug             Start Catalina in a debugger (not available on OS400)"
    echo "  debug -security   Debug Catalina with a security manager (not available on OS400)"
  else
    echo "  debug             Start Catalina in a debugger"
    echo "  debug -security   Debug Catalina with a security manager"
  fi
  echo "  jpda start        Start Catalina under JPDA debugger"
  echo "  run               Start Catalina in the current window"
  echo "  run -security     Start in the current window with security manager"
  echo "  start             Start Catalina in a separate window"
  echo "  start -security   Start in a separate window with security manager"
  echo "  stop              Stop Catalina, waiting up to 5 seconds for the process to end"
  echo "  stop n            Stop Catalina, waiting up to n seconds for the process to end"
  echo "  stop -force       Stop Catalina, wait up to 5 seconds and then use kill -KILL if still running"
  echo "  stop n -force     Stop Catalina, wait up to n seconds and then use kill -KILL if still running"
  echo "  version           What version of tomcat are you running?"
  echo "Note: Waiting for the process to end and use of the -force option require that \$NOTIFY_PID is defined"
  exit 1

fi